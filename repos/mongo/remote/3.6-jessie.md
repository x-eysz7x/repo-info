## `mongo:3.6-jessie`

```console
$ docker pull mongo@sha256:a0c7d27dd95711883a08fd2af6c58de343f2337ceb29a500562dd1b8be26716d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `mongo:3.6-jessie` - linux; amd64

```console
$ docker pull mongo@sha256:f29b6b4d8f5cebdb870d05531a947181f42b1a4ea5e3f5752cd2d4ca7fdc7e1d
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **132.0 MB (132048476 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b539e1432629f72bef1b053f0942c23852e0ca47a25e02eeb79107b68cf89970`
-	Entrypoint: `["docker-entrypoint.sh"]`
-	Default Command: `["mongod"]`

```dockerfile
# Tue, 04 Sep 2018 21:20:04 GMT
ADD file:95eda454ef09779bfb9e8ba5744d0630fb6f59eb4c9174efa44804a756d15df3 in / 
# Tue, 04 Sep 2018 21:20:05 GMT
CMD ["bash"]
# Wed, 05 Sep 2018 01:46:38 GMT
RUN groupadd -r mongodb && useradd -r -g mongodb mongodb
# Wed, 05 Sep 2018 01:47:04 GMT
RUN apt-get update 	&& apt-get install -y --no-install-recommends 		ca-certificates 		jq 		numactl 	&& rm -rf /var/lib/apt/lists/*
# Wed, 05 Sep 2018 01:47:04 GMT
ENV GOSU_VERSION=1.10
# Wed, 05 Sep 2018 01:47:05 GMT
ENV JSYAML_VERSION=3.10.0
# Wed, 05 Sep 2018 01:47:39 GMT
RUN set -ex; 		apt-get update; 	apt-get install -y --no-install-recommends 		wget 	; 	rm -rf /var/lib/apt/lists/*; 		dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; 	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; 	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; 	export GNUPGHOME="$(mktemp -d)"; 	gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; 	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; 	command -v gpgconf && gpgconf --kill all || :; 	rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc; 	chmod +x /usr/local/bin/gosu; 	gosu nobody true; 		wget -O /js-yaml.js "https://github.com/nodeca/js-yaml/raw/${JSYAML_VERSION}/dist/js-yaml.js"; 		apt-get purge -y --auto-remove wget
# Wed, 05 Sep 2018 01:47:40 GMT
RUN mkdir /docker-entrypoint-initdb.d
# Wed, 05 Sep 2018 01:49:24 GMT
ENV GPG_KEYS=2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
# Wed, 05 Sep 2018 01:49:29 GMT
RUN set -ex; 	export GNUPGHOME="$(mktemp -d)"; 	for key in $GPG_KEYS; do 		gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; 	done; 	gpg --export $GPG_KEYS > /etc/apt/trusted.gpg.d/mongodb.gpg; 	command -v gpgconf && gpgconf --kill all || :; 	rm -r "$GNUPGHOME"; 	apt-key list
# Wed, 05 Sep 2018 01:49:30 GMT
ARG MONGO_PACKAGE=mongodb-org
# Wed, 05 Sep 2018 01:49:30 GMT
ARG MONGO_REPO=repo.mongodb.org
# Wed, 05 Sep 2018 01:49:30 GMT
ENV MONGO_PACKAGE=mongodb-org MONGO_REPO=repo.mongodb.org
# Wed, 05 Sep 2018 01:49:30 GMT
ENV MONGO_MAJOR=3.6
# Wed, 05 Sep 2018 01:49:30 GMT
ENV MONGO_VERSION=3.6.7
# Wed, 05 Sep 2018 01:49:31 GMT
RUN echo "deb http://$MONGO_REPO/apt/debian jessie/${MONGO_PACKAGE%-unstable}/$MONGO_MAJOR main" | tee "/etc/apt/sources.list.d/${MONGO_PACKAGE%-unstable}.list"
# Wed, 05 Sep 2018 01:50:10 GMT
RUN set -x 	&& apt-get update 	&& apt-get install -y 		${MONGO_PACKAGE}=$MONGO_VERSION 		${MONGO_PACKAGE}-server=$MONGO_VERSION 		${MONGO_PACKAGE}-shell=$MONGO_VERSION 		${MONGO_PACKAGE}-mongos=$MONGO_VERSION 		${MONGO_PACKAGE}-tools=$MONGO_VERSION 	&& rm -rf /var/lib/apt/lists/* 	&& rm -rf /var/lib/mongodb 	&& mv /etc/mongod.conf /etc/mongod.conf.orig
# Wed, 05 Sep 2018 01:50:11 GMT
RUN mkdir -p /data/db /data/configdb 	&& chown -R mongodb:mongodb /data/db /data/configdb
# Wed, 05 Sep 2018 01:50:12 GMT
VOLUME [/data/db /data/configdb]
# Wed, 05 Sep 2018 01:50:12 GMT
COPY file:432f09fb61dea3ba52524ce5b165c30e7e96b5b711f3dc56c2c47cf8a79211d3 in /usr/local/bin/ 
# Wed, 05 Sep 2018 01:50:12 GMT
ENTRYPOINT ["docker-entrypoint.sh"]
# Wed, 05 Sep 2018 01:50:12 GMT
EXPOSE 27017/tcp
# Wed, 05 Sep 2018 01:50:13 GMT
CMD ["mongod"]
```

-	Layers:
	-	`sha256:57936531d1eea907ae6c73ebe8f8b5dc71232f5a642db22e877a4f0fc6ff1516`  
		Last Modified: Tue, 04 Sep 2018 21:23:28 GMT  
		Size: 30.1 MB (30120564 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2ed5d5ed43af9444708259d1ad7c18260dde2e6b3a261fba3433f673dbd04a87`  
		Last Modified: Wed, 05 Sep 2018 01:52:25 GMT  
		Size: 2.1 KB (2090 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:593c35b024dd2acb84e7f76b626c756e6346003b71057d98a18594f2de842779`  
		Last Modified: Wed, 05 Sep 2018 01:52:25 GMT  
		Size: 2.4 MB (2351096 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:84e56f711c3d76dc3012aa117d6f71759e16a70828b945676cbf3d9afb033e9a`  
		Last Modified: Wed, 05 Sep 2018 01:52:24 GMT  
		Size: 816.5 KB (816479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c1b3b037c3b12dce0783cfb56a7238cdc76bacf2d62b5ddef085df58e3c00648`  
		Last Modified: Wed, 05 Sep 2018 01:52:25 GMT  
		Size: 115.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:732075c59cb7ad3259c92a4975e7bc7288adcfa0c096061eb0efefb2a8896857`  
		Last Modified: Wed, 05 Sep 2018 01:53:24 GMT  
		Size: 1.4 KB (1441 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6c1472157428828df2bde216988473f5ce0cd6d958d89d4e751eb0ac13be7525`  
		Last Modified: Wed, 05 Sep 2018 01:53:23 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:fad36ffa7dcb7802f0ac005d7998a54a71ab96781614bc839573e08ed2191aca`  
		Last Modified: Wed, 05 Sep 2018 01:53:49 GMT  
		Size: 98.8 MB (98752612 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4df2313dcfc2e69e557f52289a319049b1bdf4ccbc68b1e72e8baa2deed2fed0`  
		Last Modified: Wed, 05 Sep 2018 01:53:23 GMT  
		Size: 138.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c390e0630c0f481db5944b0694f7cc2893f5cdc42bce23d4c4110565f9f0664d`  
		Last Modified: Wed, 05 Sep 2018 01:53:23 GMT  
		Size: 3.7 KB (3717 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
