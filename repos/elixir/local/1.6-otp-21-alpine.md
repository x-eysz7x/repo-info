# `elixir:1.6.6-otp-21-alpine`

## Docker Metadata

- Image ID: `sha256:a03b0444a882d2d2c23b49f76cba98fb22678fe0e0c6bb1959fa47509adaedfd`
- Created: `2018-08-31T21:42:00.831947105Z`
- Virtual Size: ~ 81.84 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["iex"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `OTP_VERSION=21.0.7`
  - `ELIXIR_VERSION=v1.6.6`
  - `LANG=C.UTF-8`
