# `elixir:1.7.3-alpine`

## Docker Metadata

- Image ID: `sha256:bc00978922fee5eda3952bbea03f0d3046f9cf84e99d0166d5e54eca176d5021`
- Created: `2018-08-31T21:40:02.046972399Z`
- Virtual Size: ~ 80.83 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["iex"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `OTP_VERSION=21.0.7`
  - `ELIXIR_VERSION=v1.7.3`
  - `LANG=C.UTF-8`
