# `haxe:3.2.1-alpine3.8`

## Docker Metadata

- Image ID: `sha256:14680eb6a5fffc600f89c985b9225ec84ded40a5793478f88d08cb255b996f2c`
- Created: `2018-09-05T09:19:06.291059674Z`
- Virtual Size: ~ 63.54 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.2.0`
  - `HAXE_VERSION=3.2.1`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`
