# `haxe:3.3.0-rc.1-alpine3.7`

## Docker Metadata

- Image ID: `sha256:42eaa26a74deb2d159da3914229fb7a49c9242877fcccf4afddde056ef8df608`
- Created: `2018-09-05T09:14:14.407432359Z`
- Virtual Size: ~ 69.32 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.2.0`
  - `HAXE_VERSION=3.3.0-rc.1`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`
