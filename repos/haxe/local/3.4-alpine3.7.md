# `haxe:3.4.7-alpine3.7`

## Docker Metadata

- Image ID: `sha256:062dd3ac1aa20c3217488f6f5d876fdcc6ec75a8c48ee484698ef0d1a32c964a`
- Created: `2018-09-05T09:07:15.211292812Z`
- Virtual Size: ~ 71.73 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.2.0`
  - `HAXE_VERSION=3.4.7`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`
