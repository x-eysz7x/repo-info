# `mongo-express:0.49.0`

## Docker Metadata

- Image ID: `sha256:fcc7ce082834e680d661fed3a5e0da028c4302717362bd44db4e41e7d0fa58ff`
- Created: `2018-09-05T08:40:23.71926311Z`
- Virtual Size: ~ 97.19 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["tini","--","/docker-entrypoint.sh"]`
- Command: `["mongo-express"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=8.11.4`
  - `YARN_VERSION=1.6.0`
  - `ME_CONFIG_EDITORTHEME=default`
  - `ME_CONFIG_MONGODB_SERVER=mongo`
  - `ME_CONFIG_MONGODB_ENABLE_ADMIN=true`
  - `ME_CONFIG_BASICAUTH_USERNAME=`
  - `ME_CONFIG_BASICAUTH_PASSWORD=`
  - `VCAP_APP_HOST=0.0.0.0`
  - `MONGO_EXPRESS=0.49.0`
