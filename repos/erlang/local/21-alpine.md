# `erlang:21.0.7-alpine`

## Docker Metadata

- Image ID: `sha256:35d2acf2125149ca08af227e8f070e1365b8357fca8e66b4e2c3971a37e8c573`
- Created: `2018-08-31T21:20:23.984216911Z`
- Virtual Size: ~ 71.93 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["erl"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `OTP_VERSION=21.0.7`
