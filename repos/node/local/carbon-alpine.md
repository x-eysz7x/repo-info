# `node:8.11.4-alpine`

## Docker Metadata

- Image ID: `sha256:8adf3c3eb26cb3822cd5aa73508efed505ee858a53237c97d857dd55c52aeb23`
- Created: `2018-09-04T22:04:59.431301277Z`
- Virtual Size: ~ 68.09 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["node"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=8.11.4`
  - `YARN_VERSION=1.6.0`
