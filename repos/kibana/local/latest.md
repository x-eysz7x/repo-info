# `kibana:5.6.11`

## Docker Metadata

- Image ID: `sha256:388661dcd03e01784f48c05437c46044da3d70ba8189318fcfc8da91bfc8e4b2`
- Created: `2018-09-05T00:14:08.022787978Z`
- Virtual Size: ~ 389.62 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["kibana"]`
- Environment:
  - `PATH=/usr/share/kibana/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.10`
  - `TINI_VERSION=v0.9.0`
  - `KIBANA_VERSION=5.6.11`
