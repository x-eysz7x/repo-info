# `docker:18.09.0-ce-tp4-dind`

## Docker Metadata

- Image ID: `sha256:170cb3d0bbb731821c56829e7ceaf9130b48bff2544286155b0d3a227060f15c`
- Created: `2018-08-30T21:47:18.091152523Z`
- Virtual Size: ~ 178.30 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["dockerd-entrypoint.sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_CHANNEL=test`
  - `DOCKER_VERSION=18.09.0-ce-tp4`
  - `DIND_COMMIT=52379fa76dee07ca038624d639d9e14f4fb719ff`
