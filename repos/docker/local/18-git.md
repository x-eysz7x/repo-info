# `docker:18.06.1-ce-git`

## Docker Metadata

- Image ID: `sha256:56056d7869d8feea6ccb78951e5b6f83a59b9fa3ea5fff366f84bbd3609f91d0`
- Created: `2018-08-30T21:47:41.628168845Z`
- Virtual Size: ~ 170.81 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_CHANNEL=stable`
  - `DOCKER_VERSION=18.06.1-ce`
