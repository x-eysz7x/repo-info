# `ibmjava:8-jre-alpine`

## Docker Metadata

- Image ID: `sha256:3a52524d9e1e68ab4f15b942e7bc893a28c336cf8a3db4e0dd84cfc1710aab3f`
- Created: `2018-09-05T21:21:37.774945633Z`
- Virtual Size: ~ 207.54 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/opt/ibm/java/jre/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_VERSION=1.8.0_sr5fp21`
  - `JAVA_HOME=/opt/ibm/java/jre`
  - `IBM_JAVA_OPTIONS=-XX:+UseContainerSupport`
