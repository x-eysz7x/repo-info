# `ibmjava:8-sdk-alpine`

## Docker Metadata

- Image ID: `sha256:041ae8d40bdaa504fba009bcbec58fb6b927b5c62771660b9f08ac0fe8dee085`
- Created: `2018-09-05T21:24:39.113899174Z`
- Virtual Size: ~ 251.27 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/opt/ibm/java/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_VERSION=1.8.0_sr5fp21`
  - `JAVA_HOME=/opt/ibm/java/jre`
  - `IBM_JAVA_OPTIONS=-XX:+UseContainerSupport`
