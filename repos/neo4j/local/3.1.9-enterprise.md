# `neo4j:3.1.9-enterprise`

## Docker Metadata

- Image ID: `sha256:475813977ab5ab919de710b21134e5281f383dbe456e5af842b9a44322460444`
- Created: `2018-09-05T13:40:12.221330138Z`
- Virtual Size: ~ 190.42 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/sbin/tini","-g","--","/docker-entrypoint.sh"]`
- Command: `["neo4j"]`
- Environment:
  - `PATH=/var/lib/neo4j/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/jre`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `NEO4J_SHA256=fe0764ec5b4799da1128540eb21bdf19738fa4e73d5e6a5c55055ff7ad402084`
  - `NEO4J_TARBALL=neo4j-enterprise-3.1.9-unix.tar.gz`
