# `neo4j:3.2.12-enterprise`

## Docker Metadata

- Image ID: `sha256:b9d4430189b8d194c8a3f86737859423853e728ab7b0b374ca7a525000d36b42`
- Created: `2018-09-05T13:27:44.997885917Z`
- Virtual Size: ~ 193.94 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/sbin/tini","-g","--","/docker-entrypoint.sh"]`
- Command: `["neo4j"]`
- Environment:
  - `PATH=/var/lib/neo4j/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/jre`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `NEO4J_SHA256=5fea42ac86610a52d675637b4f62ef3c1b17ab807ef97889c7f1e84673becb1e`
  - `NEO4J_TARBALL=neo4j-enterprise-3.2.12-unix.tar.gz`
