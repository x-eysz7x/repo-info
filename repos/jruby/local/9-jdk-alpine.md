# `jruby:9-jdk-alpine`

## Docker Metadata

- Image ID: `sha256:bf775f59fb89dab195c06d0c4ca78d03c069b8ce431dbd391afd0d623b7fb002`
- Created: `2018-09-05T13:02:54.32445411Z`
- Virtual Size: ~ 145.02 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["irb"]`
- Environment:
  - `PATH=/usr/local/bundle/bin:/opt/jruby/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `JRUBY_VERSION=9.2.0.0`
  - `JRUBY_SHA256=42718dea5fc90b7696cb3fccf8e8d546729173963ad0bc477d66545677d00684`
  - `GEM_HOME=/usr/local/bundle`
  - `BUNDLE_PATH=/usr/local/bundle`
  - `BUNDLE_BIN=/usr/local/bundle/bin`
  - `BUNDLE_SILENCE_ROOT_WARNING=1`
  - `BUNDLE_APP_CONFIG=/usr/local/bundle`
