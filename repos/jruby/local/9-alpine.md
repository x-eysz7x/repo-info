# `jruby:9-alpine`

## Docker Metadata

- Image ID: `sha256:4c28651b5373b952604487775027323c3507b197dab0ed0c160d4f9ec76afa40`
- Created: `2018-09-05T13:00:53.826415565Z`
- Virtual Size: ~ 125.41 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["irb"]`
- Environment:
  - `PATH=/usr/local/bundle/bin:/opt/jruby/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/jre`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `JRUBY_VERSION=9.2.0.0`
  - `JRUBY_SHA256=42718dea5fc90b7696cb3fccf8e8d546729173963ad0bc477d66545677d00684`
  - `GEM_HOME=/usr/local/bundle`
  - `BUNDLE_PATH=/usr/local/bundle`
  - `BUNDLE_BIN=/usr/local/bundle/bin`
  - `BUNDLE_SILENCE_ROOT_WARNING=1`
  - `BUNDLE_APP_CONFIG=/usr/local/bundle`
