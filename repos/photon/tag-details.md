<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `photon`

-	[`photon:1.0`](#photon10)
-	[`photon:1.0-20180816`](#photon10-20180816)
-	[`photon:2.0`](#photon20)
-	[`photon:2.0-20180823`](#photon20-20180823)
-	[`photon:dev`](#photondev)
-	[`photon:dev-20180904`](#photondev-20180904)
-	[`photon:latest`](#photonlatest)

## `photon:1.0`

```console
$ docker pull photon@sha256:1980a0145646578bf6f33ad6e5369ead92ad21b2e687b94b3f93ed868333ab61
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `photon:1.0` - linux; amd64

```console
$ docker pull photon@sha256:e9ac9d29cf8ba165acb05965bc67eb4b1dde24eb5d82476838d5acdbe283227c
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.3 MB (49279396 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a37141d9340deae80b2bba8cc4fb3c553b0caaf6d1383210aa85269feb137c5`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:36 GMT
ADD file:d4d0fc7d1b658f3bc2bfa28b658f5596e5cba900fc90c93d6ae0932d2087809f in / 
# Thu, 06 Sep 2018 18:20:37 GMT
LABEL name=Photon OS 1.0 Base Image vendor=VMware build-date=20180816
# Thu, 06 Sep 2018 18:20:37 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:51be32cd3c9d6d989880318de64d5b7488d0412264d4ba1ce7aa9eee3ddc91b9`  
		Last Modified: Thu, 16 Aug 2018 20:47:53 GMT  
		Size: 49.3 MB (49279396 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:1.0-20180816`

```console
$ docker pull photon@sha256:1980a0145646578bf6f33ad6e5369ead92ad21b2e687b94b3f93ed868333ab61
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `photon:1.0-20180816` - linux; amd64

```console
$ docker pull photon@sha256:e9ac9d29cf8ba165acb05965bc67eb4b1dde24eb5d82476838d5acdbe283227c
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.3 MB (49279396 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a37141d9340deae80b2bba8cc4fb3c553b0caaf6d1383210aa85269feb137c5`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:36 GMT
ADD file:d4d0fc7d1b658f3bc2bfa28b658f5596e5cba900fc90c93d6ae0932d2087809f in / 
# Thu, 06 Sep 2018 18:20:37 GMT
LABEL name=Photon OS 1.0 Base Image vendor=VMware build-date=20180816
# Thu, 06 Sep 2018 18:20:37 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:51be32cd3c9d6d989880318de64d5b7488d0412264d4ba1ce7aa9eee3ddc91b9`  
		Last Modified: Thu, 16 Aug 2018 20:47:53 GMT  
		Size: 49.3 MB (49279396 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:2.0`

```console
$ docker pull photon@sha256:9739fa19b381ff5614f6edf6d62e4701e72a918874bbe67337cb41e30743d0ef
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `photon:2.0` - linux; amd64

```console
$ docker pull photon@sha256:9c041ffe672617820c2eb9c7882a8312b55f74206baec1d8c08ccdf785201150
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.1 MB (14137907 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:31e9b6a7c79b9affc461d503816d40c613e6b5895d4ba56ae9a407bd1ca67504`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:05 GMT
ADD file:95b1237e6ca93380f6d7910b7bcc3d67330e7e322d2b37e39c8eaac7a7b4e8a0 in / 
# Thu, 06 Sep 2018 18:20:05 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20180823
# Thu, 06 Sep 2018 18:20:06 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7672e645826b09813fe3378442ef1a023ba9c6d3b73caec6cd14116254d439e6`  
		Last Modified: Thu, 23 Aug 2018 21:21:08 GMT  
		Size: 14.1 MB (14137907 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:2.0-20180823`

```console
$ docker pull photon@sha256:9739fa19b381ff5614f6edf6d62e4701e72a918874bbe67337cb41e30743d0ef
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `photon:2.0-20180823` - linux; amd64

```console
$ docker pull photon@sha256:9c041ffe672617820c2eb9c7882a8312b55f74206baec1d8c08ccdf785201150
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.1 MB (14137907 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:31e9b6a7c79b9affc461d503816d40c613e6b5895d4ba56ae9a407bd1ca67504`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:05 GMT
ADD file:95b1237e6ca93380f6d7910b7bcc3d67330e7e322d2b37e39c8eaac7a7b4e8a0 in / 
# Thu, 06 Sep 2018 18:20:05 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20180823
# Thu, 06 Sep 2018 18:20:06 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7672e645826b09813fe3378442ef1a023ba9c6d3b73caec6cd14116254d439e6`  
		Last Modified: Thu, 23 Aug 2018 21:21:08 GMT  
		Size: 14.1 MB (14137907 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:dev`

```console
$ docker pull photon@sha256:c27e22d3dc01e7891342667644822fab9fb3ef7611832bfc66c09c6515039e7b
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:dev` - linux; amd64

```console
$ docker pull photon@sha256:24a6b0334d98fa6feb9e2a6948ec081ff31e9f6a5e314b53eb54e6a8da169719
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.1 MB (14103334 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:967f4cae7b06dff9201d7576364b0f959673f1ae89964f72eb660567064ba1fb`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:53 GMT
ADD file:200dac7aaa4285b4c10d60b01b3f28638e24183c6872a3377e5dd993883ef329 in / 
# Thu, 06 Sep 2018 18:20:53 GMT
LABEL name=Photon OS Dev Base Image vendor=VMware build-date=20180901
# Thu, 06 Sep 2018 18:20:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:215e961a2b01876fe288bdcd8336de92de6b725a5935683e412af651a1052835`  
		Last Modified: Thu, 06 Sep 2018 18:21:43 GMT  
		Size: 14.1 MB (14103334 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:dev` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:21d2fb56f6ea87a6754f39516e27c61b21ee13254b96b5c091ba637c2ebcc12a
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **11.9 MB (11925621 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d138e96320f712653ad937a0b49a84d630879723e567cca2b9c9e8b3beb61186`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 07 Sep 2018 09:00:02 GMT
ADD file:eabc754ac1af7564c69f045fe0b65a491327479bf83e0c41294fac7b44a66808 in / 
# Fri, 07 Sep 2018 09:00:02 GMT
LABEL name=Photon OS Dev Base Image vendor=VMware build-date=20180904
# Fri, 07 Sep 2018 09:00:03 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6126c050f45ccac0e88b69059e4c8ea579ecc39243b8f88ad14d964a260f4799`  
		Last Modified: Fri, 07 Sep 2018 09:01:27 GMT  
		Size: 11.9 MB (11925621 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:dev-20180904`

```console
$ docker pull photon@sha256:c27e22d3dc01e7891342667644822fab9fb3ef7611832bfc66c09c6515039e7b
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:dev-20180904` - linux; amd64

```console
$ docker pull photon@sha256:24a6b0334d98fa6feb9e2a6948ec081ff31e9f6a5e314b53eb54e6a8da169719
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.1 MB (14103334 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:967f4cae7b06dff9201d7576364b0f959673f1ae89964f72eb660567064ba1fb`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:53 GMT
ADD file:200dac7aaa4285b4c10d60b01b3f28638e24183c6872a3377e5dd993883ef329 in / 
# Thu, 06 Sep 2018 18:20:53 GMT
LABEL name=Photon OS Dev Base Image vendor=VMware build-date=20180901
# Thu, 06 Sep 2018 18:20:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:215e961a2b01876fe288bdcd8336de92de6b725a5935683e412af651a1052835`  
		Last Modified: Thu, 06 Sep 2018 18:21:43 GMT  
		Size: 14.1 MB (14103334 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:dev-20180904` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:21d2fb56f6ea87a6754f39516e27c61b21ee13254b96b5c091ba637c2ebcc12a
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **11.9 MB (11925621 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d138e96320f712653ad937a0b49a84d630879723e567cca2b9c9e8b3beb61186`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 07 Sep 2018 09:00:02 GMT
ADD file:eabc754ac1af7564c69f045fe0b65a491327479bf83e0c41294fac7b44a66808 in / 
# Fri, 07 Sep 2018 09:00:02 GMT
LABEL name=Photon OS Dev Base Image vendor=VMware build-date=20180904
# Fri, 07 Sep 2018 09:00:03 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6126c050f45ccac0e88b69059e4c8ea579ecc39243b8f88ad14d964a260f4799`  
		Last Modified: Fri, 07 Sep 2018 09:01:27 GMT  
		Size: 11.9 MB (11925621 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:latest`

```console
$ docker pull photon@sha256:9739fa19b381ff5614f6edf6d62e4701e72a918874bbe67337cb41e30743d0ef
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `photon:latest` - linux; amd64

```console
$ docker pull photon@sha256:9c041ffe672617820c2eb9c7882a8312b55f74206baec1d8c08ccdf785201150
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.1 MB (14137907 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:31e9b6a7c79b9affc461d503816d40c613e6b5895d4ba56ae9a407bd1ca67504`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:05 GMT
ADD file:95b1237e6ca93380f6d7910b7bcc3d67330e7e322d2b37e39c8eaac7a7b4e8a0 in / 
# Thu, 06 Sep 2018 18:20:05 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20180823
# Thu, 06 Sep 2018 18:20:06 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7672e645826b09813fe3378442ef1a023ba9c6d3b73caec6cd14116254d439e6`  
		Last Modified: Thu, 23 Aug 2018 21:21:08 GMT  
		Size: 14.1 MB (14137907 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
