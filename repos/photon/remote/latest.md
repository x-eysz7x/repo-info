## `photon:latest`

```console
$ docker pull photon@sha256:9739fa19b381ff5614f6edf6d62e4701e72a918874bbe67337cb41e30743d0ef
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `photon:latest` - linux; amd64

```console
$ docker pull photon@sha256:9c041ffe672617820c2eb9c7882a8312b55f74206baec1d8c08ccdf785201150
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.1 MB (14137907 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:31e9b6a7c79b9affc461d503816d40c613e6b5895d4ba56ae9a407bd1ca67504`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 06 Sep 2018 18:20:05 GMT
ADD file:95b1237e6ca93380f6d7910b7bcc3d67330e7e322d2b37e39c8eaac7a7b4e8a0 in / 
# Thu, 06 Sep 2018 18:20:05 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20180823
# Thu, 06 Sep 2018 18:20:06 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7672e645826b09813fe3378442ef1a023ba9c6d3b73caec6cd14116254d439e6`  
		Last Modified: Thu, 23 Aug 2018 21:21:08 GMT  
		Size: 14.1 MB (14137907 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
