# `plone:4.3.17-alpine`

## Docker Metadata

- Image ID: `sha256:af4140b1714c26f145043c0afabcb891b7f752602038fa8fb8ef239c1647094b`
- Created: `2018-09-05T18:08:35.615232913Z`
- Virtual Size: ~ 314.35 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["start"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `LANG=C.UTF-8`
  - `PYTHONIOENCODING=UTF-8`
  - `GPG_KEY=C01E1CAD5EA2C4F0B8E3571504C367C218ADD4FF`
  - `PYTHON_VERSION=2.7.15`
  - `PYTHON_PIP_VERSION=18.0`
  - `PIP=9.0.3`
  - `ZC_BUILDOUT=2.11.2`
  - `SETUPTOOLS=38.7.0`
  - `PLONE_MAJOR=4.3`
  - `PLONE_VERSION=4.3.17`
  - `PLONE_MD5=4d9f175803c63301758f42dbf68f015a`
