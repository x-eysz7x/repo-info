# `httpd:2.4.34-alpine`

## Docker Metadata

- Image ID: `sha256:9af95015a092e8dbbe8da16155fd67862692b446cc21dc6064e6d231c51bb542`
- Created: `2018-09-05T00:07:13.981395505Z`
- Virtual Size: ~ 91.39 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["httpd-foreground"]`
- Environment:
  - `PATH=/usr/local/apache2/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `HTTPD_PREFIX=/usr/local/apache2`
  - `HTTPD_VERSION=2.4.34`
  - `HTTPD_SHA256=fa53c95631febb08a9de41fd2864cfff815cf62d9306723ab0d4b8d7aa1638f0`
  - `HTTPD_PATCHES=`
  - `APACHE_DIST_URLS=https://www.apache.org/dyn/closer.cgi?action=download&filename= 	https://www-us.apache.org/dist/ 	https://www.apache.org/dist/ 	https://archive.apache.org/dist/`
