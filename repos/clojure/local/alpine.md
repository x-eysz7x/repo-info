# `clojure:lein-2.8.1-alpine`

## Docker Metadata

- Image ID: `sha256:b1be6630061eecb8a35cd042fdc9b1d06fbd2b9dd4ecae13dbe3914a25169179`
- Created: `2018-09-05T12:07:09.908896645Z`
- Virtual Size: ~ 140.41 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin:/usr/local/bin/`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `LEIN_VERSION=2.8.1`
  - `LEIN_INSTALL=/usr/local/bin/`
  - `LEIN_ROOT=1`
