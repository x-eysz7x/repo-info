# `kapacitor:1.4-alpine`

## Docker Metadata

- Image ID: `sha256:7d6ef2f8337692ec0c34058c4d740059ef9e414958b6f5a4471f2c64adf28093`
- Created: `2018-09-05T11:03:52.850473404Z`
- Virtual Size: ~ 64.21 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["kapacitord"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `KAPACITOR_VERSION=1.4.1`
